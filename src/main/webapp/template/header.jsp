<%@ page import="es.segui.modelo.entidades.Tipo" %>
<%@ page import="es.segui.modelo.entidades.produccion.Produccion" %>
<%@ page import="es.segui.modelo.entidades.Genero" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="es.segui.modelo.entidades.Usuario" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<header>
  <%
    ArrayList<Genero> generos = (ArrayList<Genero>) request.getAttribute("generos");%>
  <% Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");%>
  <div class="top">
    <div class="logo"> <a href="lista-producciones"><img src="img/favicon.jpg" alt="logo" class="logo-img"> </a></div>
    <div class="nav">
      <nav id="list">
        <ul id="nav">
          <li><a href="busqueda-filtrada?tipo=<%=Tipo.MOVIE.name()%>&orden=DESC&page=0"><b>Películas</b></a></li>
          <li><a href="busqueda-filtrada?tipo=<%=Tipo.TVSHOW.name()%>&orden=DESC&page=0"><b>Series</b></a></li>
          <% if (usuario != null){ %>
          <li><a href="lista-favoritas?orden=DESC&page=0"><b>Favoritos</b></a></li>
          <% } %>
        </ul>
      </nav>
    </div>
    <div class="select">
      <% if (usuario == null){ %>
      <a href="login">Iniciar sesión </a>
      <% } else { %>
      <p><%=usuario.getEmail()%></p>
      <a href="logout">Cerrar sesión </a>
      <% } %>
    </div>
    <div class="search-div">
      <form action="busqueda-filtrada" method="get" id="buscar">
        <label><input type="search" id="gsearch" placeholder="Busqueda" name="search"/></label>
        <input type="hidden" name="orden" value="DESC">
        <label >Genero
          <select id="tematica" name="tematica">
            <option value="">Todos</option>
            <% for (Genero genero : generos) {%>
            <option value="<%=genero.getId()%>"><%=genero.getDescripcion()%></option>
            <% } %>
          </select>
        </label>
        <input type="hidden" name="page" value="0">
      </form>

    </div>
  </div>
</header>
</html>
