<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="es.segui.modelo.entidades.produccion.Produccion" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="es.segui.modelo.entidades.Usuario" %>
<%@ page import="es.segui.modelo.entidades.produccion.Serie" %>
<%@ page import="java.util.HashMap" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="img/favicon.jpg">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Realizar búsqueda</title>
</head>
<body>
<jsp:include page="template/header.jsp"/>
<%ArrayList<Produccion> produciones = (ArrayList<Produccion>) request.getAttribute("lista");
	HashMap<Integer,Float> scores = (HashMap<Integer, Float>) request.getAttribute("scores");
	String enlace = (String) request.getAttribute("enlace");
	String tipo = (String) request.getAttribute("tipo");
	int numeroPaginas = (int) request.getAttribute("paginas");
	String pagina = request.getQueryString();
	pagina = pagina.replaceFirst("page=\\d*","");
	Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
	ArrayList<Integer> listaFav = (ArrayList<Integer>) request.getAttribute("listadoFavoritos");
	String url = request.getQueryString();
	String orden = "";
	if (url.contains("DESC")){
		 orden = url.replaceFirst("orden=DESC","orden=ASC");
	} else {
		 orden = url.replaceFirst("orden=ASC","orden=DESC");
	}
	orden = orden.replaceFirst("page=\\d*","page=0");

%>
	<div id="search">
		<h1><%=tipo%></h1>
		<% if (orden.contains("ASC")){%>
		<h4> MAS Nuevas <a href="<%=enlace + orden%>">cambiar</a></h4>
		<%} else {%>
		<h4> MAS Viejas <a href="<%=enlace + orden%>">cambiar</a></h4>
		<%}%>
		<%for (Produccion lista : produciones) {%>
		<div class="film">
			<div class="contenedor2">
				<img src="<%= lista.getPortada()%>" class="picture" onerror="this.src='img/imagen-no-encontrada.jpg'">
				<div class="contenedor">
					<img src="img/star.png" alt="scores" class="score">
					<%if (scores.containsKey(lista.getId())){%>
					<div class="texto"> <%=scores.get(lista.getId())%></div>
					<%}else {%>
					<div class="texto">0.0</div>
					<%}%>
				</div>
			</div>
			<% if (usuario != null) { %>
			<form action="favoritos" method="post" class="favoritos">
				<input type="hidden" value="<%=usuario.getEmail()%>" name="correo">
				<% if (listaFav.contains(lista.getId())) {%>
				<button class="favoritos selected" value="<%=lista.getId()%>" type="submit" name="id-fav">
				</button>
				<% } else { %>
				<button class="favoritos noselected" value="<%=lista.getId()%>" type="submit" name="id-fav">
				</button>
				<% } %>
			</form>
			<% } %>
			<form action="show-details" method="get" class="detalles">
				<%if (lista instanceof Serie){%>
				<input type="hidden" value="0" name="page">
				<%}%>
				<button value="<%=lista.getId()%>" type="submit" name="id">Detalle</button>
			</form>
		</div>
		<% } %>
		<br>
		<h4>Paginas: <%
			for (int i = 0; i < numeroPaginas; i++) {  %>
				<a href="<%=enlace + pagina + "page=" + i%>" ><%= i %></a>
			<% } %></h4>
	</div>
</body>
</html>