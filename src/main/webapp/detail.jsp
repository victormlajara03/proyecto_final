<%@ page import="es.segui.modelo.entidades.produccion.Produccion" %>
<%@ page import="es.segui.modelo.entidades.Usuario" %>
<%@ page import="es.segui.modelo.entidades.produccion.Serie" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="es.segui.modelo.entidades.Temporada" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="img/favicon.jpg">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Detalles de la película</title>
</head>
<body>
<% Produccion produccion = (Produccion) request.getAttribute("produccion");
	HashMap<Integer,Float> scores = (HashMap<Integer, Float>) request.getAttribute("scores ");
	ArrayList<Temporada> temporadas = null;
	Integer pagina = null;
	String url = request.getQueryString();

	if (produccion instanceof Serie){
		temporadas = (ArrayList<Temporada>) request.getAttribute("temporadas");
		pagina = (int) request.getAttribute("pagina");

	}%>
<jsp:include page="template/header.jsp"/>
	<div id="detail">
		<div id="background">
			<%Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");%>
			<div id="description">
				<p><strong>Título:</strong> <%=produccion.getTitulo()%></p>
				<p><strong>Dirección:</strong>
					<%
						for (int i = 0; i <produccion.getDirector().size() ; i++) { %>
					<%=produccion.getDirector().get(i).getNombre()%>
					<%if (i < produccion.getDirector().size() -1) { %>
					,
					<% } %>
					<%  } %>
				</p>
				<p><strong>Reparto:</strong>
					<%
						for (int i = 0; i <produccion.getActor().size() ; i++) { %>
					        <%=produccion.getActor().get(i).getName()%>
							<%if (i < produccion.getActor().size() -1) { %>
								,
						 <% } %>
					<%  } %>
				</p>
				<p><strong>Guión:</strong> <%=produccion.getGuion()%>
					<%if (produccion.getWeb() != null){%>
					<a href="<%= produccion.getWeb()%>">Mas información </a>
					<%}%></p>
				<p><strong>Géneros:</strong>
					<%
						for (int i = 0; i <produccion.getGenero().size() ; i++) { %>
					<%=produccion.getGenero().get(i).getDescripcion()%>
					<%if (i < produccion.getGenero().size() -1) { %>
					,
						<%}%>
						<%}%>
				<% if (temporadas != null){%>
				<p>Año: <%= temporadas.get(pagina).getAnyoLanzamiento()%>. <%=temporadas.get(0).getNumCapitulos()%> Capitulos </p>
				<p><b>Guion Temporada:</b> <%=temporadas.get(pagina).getGuion()%></p>
				<% for (int i = 0; i < temporadas.size(); i++) {
					url = url.replaceFirst("page=\\d*","page="+i);%>

				<a href="show-details?<%=url%>" ><%= i+1 %></a>
				<%}%>
				<% } %>
				<p><strong>Idiomas:</strong>
					<%
						for (int i = 0; i <produccion.getIdioma().size() ; i++) { %>
					<%=produccion.getIdioma().get(i).getDescripcion()%>
					<%if (i < produccion.getIdioma().size() -1) { %>
					,
					<% } %>
					<%  } %>
				</p>
				<p><strong>Productora:</strong> <%=produccion.getProductora().getNombre()%></p>
				<p><strong>Calificación:</strong> <%=produccion.getCalificacion().getNombre()%></p>
				<p> <strong>Plataformas:</strong>
				<%
					for (int i = 0; i <produccion.getPlataforma().size() ; i++) { %>
				<%= produccion.getPlataforma().get(i).getNombre()%>
					<%if (i < produccion.getPlataforma().size() -1) { %>
					,
					<% } %>
				<% } %>
				</p>
			</div>
			<div id="detail-picture">
				<img src="<%=produccion.getPortada()%>" class="picture" alt="imagen" onerror="this.src='img/imagen-no-encontrada.jpg'">
				<div class="contenedor">
					<img src="img/star.png" alt="scores" class="score">
					<%if (scores.containsKey(produccion.getId())){%>
					<div class="texto"> <%=scores.get(produccion.getId())%></div>
					<%}else {%>
					<div class="texto">0.0</div>
					<%}%>
				</div>

				<% if (usuario != null) { %>
				<div id="valuation">
					<form action="show-details" method="post" id="valuate">
						<input type="radio" id="radio1" value="5" name="estrellas" class="star">
						<label for="radio1">&#9733;</label>
						<input type="radio" id="radio2" value="4" name="estrellas" class="star">
						<label for="radio2">&#9733;</label>
						<input type="radio" id="radio3" value="3" name="estrellas" class="star">
						<label for="radio3">&#9733;</label>
						<input type="radio" id="radio4" value="2" name="estrellas" class="star">
						<label for="radio4">&#9733;</label>
						<input type="radio" id="radio5" value="1" name="estrellas" class="star">
						<label for="radio5">&#9733;</label>
						<input type="hidden" value="<%=usuario.getEmail()%>" name="correo">
						<input type="hidden" value="<%=produccion.getId()%>" name="id">
						<input type="hidden" id="pagina" value="<%=request.getQueryString()%>" name="enlace">
						<input type="submit" id="valuate1" value="Valorar">
					</form>
				</div>
				<% } %>
			</div>
		</div>
	</div>
</body>
</html>