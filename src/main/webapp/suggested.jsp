<%@ page import="java.util.ArrayList" %>
<%@ page import="es.segui.modelo.entidades.produccion.Produccion" %>
<%@ page import="es.segui.modelo.entidades.Tipo" %>
<%@ page import="es.segui.modelo.entidades.Genero" %>
<%@ page import="es.segui.modelo.entidades.Usuario" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="img/favicon.jpg">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>BatoiFlix</title>
</head>
<body>
<jsp:include page="template/header.jsp"/>
<% ArrayList<Produccion> pelis = (ArrayList<Produccion>) request.getAttribute("peliculas");
	ArrayList<Produccion> series = (ArrayList<Produccion>) request.getAttribute("series");
	Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
	ArrayList<Integer> listaFav = (ArrayList<Integer>) request.getAttribute("listadoFavoritos");
	HashMap<Integer,Float> scores = (HashMap<Integer, Float>) request.getAttribute("scores ");%>
<div id="recommended">
	<h1>Mas recomendadas</h1>
	<h2>Películas</h2>
	<div id="films">
		<%for (Produccion peli : pelis) {%>
		<div class="film">
			<div class="contenedor2">
				<img src="<%= peli.getPortada()%>" class="picture" onerror="this.src='img/imagen-no-encontrada.jpg'">
				<div class="contenedor">
					<img src="img/star.png" alt="scores" class="score">
					<%if (scores.containsKey(peli.getId())){%>
					<div class="texto"> <%=scores.get(peli.getId())%></div>
					<%}else {%>
					<div class="texto">0.0</div>
					<%}%>
				</div>
			</div>
			<% if (usuario != null) { %>
			<form action="favoritos" method="post" class="favoritos">
				<input type="hidden" value="<%=usuario.getEmail()%>" name="correo">
					<% if (listaFav.contains(peli.getId())) {%>
					<button class="favoritos selected" value="<%=peli.getId()%>" type="submit" name="id-fav">
					</button>
					<% } else { %>
					<button class="favoritos noselected" value="<%=peli.getId()%>" type="submit" name="id-fav">
					</button>
					<% } %>
			</form>
			<% } %>
			<form action="show-details" method="get" class="detalles">
				<button value="<%=peli.getId()%>" type="submit" name="id">Detalle</button>
			</form>
		</div>
		<% } %>
	</div>
	<h2>Series</h2>
	<div id="series">
		<%for (Produccion serie : series) {%>
		<div class="film">
			<div class="contenedor2">
				<img src="<%= serie.getPortada()%>" class="picture" onerror="this.src='img/imagen-no-encontrada.jpg'">
				<div class="contenedor">
					<img src="img/star.png" alt="scores" class="score">
					<%if (scores.containsKey(serie.getId())){%>
					<div class="texto"> <%=scores.get(serie.getId())%></div>
					<%}else {%>
					<div class="texto">0.0</div>
					<%}%>
				</div>
			</div>
			<% if (usuario != null) { %>
			<form action="favoritos" method="post" class="favoritos">
				<input type="hidden" value="<%=usuario.getEmail()%>" name="correo">
					<% if (listaFav.contains(serie.getId())) {%>
				<button class="favoritos selected" value="<%=serie.getId()%>" type="submit" name="id-fav">
				</button>
				<% } else { %>
				<button class="favoritos noselected" value="<%=serie.getId()%>" type="submit" name="id-fav">
				</button>
				<% } %>
			</form>
			<% } %>
			<form action="show-details" method="get" class="detalles">
				<input type="hidden" value="0" name="page">
				<button value="<%=serie.getId()%>" type="submit" name="id">Detalle</button>
			</form>
		</div>
		<% } %>
	</div>
</div>
</body>
</html>