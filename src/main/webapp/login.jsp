<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="img/favicon.jpg">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Iniciar sesión</title>
</head>
<body>
<jsp:include page="template/header.jsp"/>
	<div id="login">
		<form id="login-form" action="login" method="post">
			Correo:
				<br>
			<input class="input" type="email" name="email">
			<br>
			<br>
			Contraseña:
			<br>
			<input class="input" type="password" name="password">
			<br>
			<br>
			<input id="login-button" type="submit" name="submit" value="Iniciar sesión">
			<%
				if( request.getSession().getAttribute("message") != null) {
					out.println("<p>" + request.getSession().getAttribute("message") + "</p>");
					request.getSession().removeAttribute("message");
				}%>
		</form>
	</div>
</body>
</html>