package es.segui.modelo.repositorio;

import es.segui.modelo.dao.*;
import es.segui.modelo.dao.core.SQLGenericDAO;
import es.segui.modelo.entidades.*;
import es.segui.modelo.entidades.produccion.Produccion;

import java.util.ArrayList;

public class ProduccionRepositorio {

    private SQLProduccionDAO sqlProduccionDAO;

    private SQLGeneroDAO sqlGeneroDAO;

    public ProduccionRepositorio() {
        this.sqlProduccionDAO = new SQLProduccionDAO();
        this.sqlGeneroDAO = new SQLGeneroDAO();
    }

    public Produccion getByIdPopulateAll(int id) {
        Produccion produccion = sqlProduccionDAO.findById(id);
        populateAggregates(produccion);
        return produccion;
    }

    private void populateAggregates(Produccion produccion){
        populateGenero(produccion);
        populateArctor(produccion);
        populateDirectors(produccion);
        populateLenguages(produccion);
        populateProductora(produccion);
        populateCalificacion(produccion);
        populatePlataforma(produccion);
    }

    private void populateGenero(Produccion produccion){
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        ArrayList<Genero> generos = sqlGeneroDAO.findByIdProd(produccion.getId());
        produccion.setGenero(generos);
    }

    private void populateArctor(Produccion produccion){
        SQLActorDAO sqlActorDAO = new SQLActorDAO();
        ArrayList<Actor> actors = sqlActorDAO.findByIdProd(produccion.getId());
        produccion.setActor(actors);
    }

    private void populateDirectors(Produccion produccion){
        SQLDirectorDAO sqlDirectorDAO = new SQLDirectorDAO();
        ArrayList<Director> directors = sqlDirectorDAO.findByIdProd(produccion.getId());
        produccion.setDirector(directors);
    }

    private void populateLenguages(Produccion produccion){
        SQLIdiomaDAO sqlIdiomaDAO = new SQLIdiomaDAO();
        ArrayList<Idioma> directors = sqlIdiomaDAO.findByIdProd(produccion.getId());
        produccion.setIdioma(directors);
    }

    private void populateProductora(Produccion produccion){
        SQLProductoraDAO sqlProductoraDAO = new SQLProductoraDAO();
        Productora productora = sqlProductoraDAO.findByIdProd(produccion.getId());
        produccion.setProductora(productora);
    }

    private void populateCalificacion(Produccion produccion){
        SQLCalificacionDAO sqlProductoraDAO = new SQLCalificacionDAO();
        Calificacion productora = sqlProductoraDAO.findByIdProd(produccion.getId());
        produccion.setCalificacion(productora);
    }

    private void populatePlataforma(Produccion produccion){
        SQLPlataformaDAO sqlPlataformaDAO = new SQLPlataformaDAO();
        ArrayList<Plataforma> plataformas = sqlPlataformaDAO.findByIdProd(produccion.getId());
        produccion.setPlataforma(plataformas);
    }

}
