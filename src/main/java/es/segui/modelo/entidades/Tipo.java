package es.segui.modelo.entidades;

public enum Tipo {
    MOVIE, TVSHOW;

    public static Tipo fromString(String tipo) {
        if(tipo.equalsIgnoreCase("movie")) {
            return MOVIE;
        } else if(tipo.equalsIgnoreCase("tvshow") || tipo.equalsIgnoreCase("tv-show")) {
            return TVSHOW;
        }

        throw new RuntimeException("Invalid Type conversion");
    }
}
