package es.segui.modelo.entidades;

public class Idioma {

    private int id;

    private String cod;

    private String descripcion;

    public Idioma(int id, String cod, String descripcion) {
        this.id = id;
        this.cod = cod;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public String getCod() {
        return cod;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
