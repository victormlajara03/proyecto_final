package es.segui.modelo.entidades;

public class Plataforma {

    private int cod;

    private String nombre;

    public Plataforma(int cod, String nombre) {
        this.cod = cod;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
}
