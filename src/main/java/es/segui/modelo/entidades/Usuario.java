package es.segui.modelo.entidades;

public class Usuario {

    private String email;

    private String nombre;

    private String password;

    private int tipo;

    public Usuario(String email, String nombre, String password, int tipo) {
        this.email = email;
        this.nombre = nombre;
        this.password = password;
        this.tipo = tipo;
    }

    public String getEmail() {
        return email;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPassword() {
        return password;
    }

    public int getTipo() {
        return tipo;
    }
}
