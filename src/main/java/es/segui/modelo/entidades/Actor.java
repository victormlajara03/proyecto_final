package es.segui.modelo.entidades;

public class Actor {

    private int cod;

    private String name;

    public Actor(int cod, String name) {
        this.cod = cod;
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public String getName() {
        return name;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
}
