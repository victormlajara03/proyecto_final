package es.segui.modelo.entidades;

public class Productora {

    private int cod;

    private String nombre;

    public Productora(int cod, String nombre) {
        this.cod = cod;
        this.nombre = nombre;
    }

    public int getCod() {
        return cod;
    }

    public String getNombre() {
        return nombre;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
}
