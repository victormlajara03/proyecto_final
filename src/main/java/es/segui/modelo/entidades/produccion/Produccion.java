package es.segui.modelo.entidades.produccion;

import es.segui.modelo.entidades.*;

import java.time.LocalDate;
import java.util.ArrayList;

public abstract class Produccion {

    private int id;

    private String titulo;

    private Calificacion calificacion;

    private LocalDate anyoLanzamineto;

    private int duracion;

    private ArrayList<Genero> genero;

    private ArrayList<Director> director;

    private ArrayList<String> escritor;

    private ArrayList<Actor> actor;

    private String guion;

    private ArrayList<Idioma> idioma;

    private String portada;

    private Tipo tipo;

    private Productora productora;

    private String web;

    private ArrayList<Plataforma> plataforma;

    private int numVisitas;

    public Produccion(int id, String titulo, LocalDate anyoLanzamineto,
                      int duracion, String guion, String portada, Tipo tipo, String web) {
        this.id = id;
        this.titulo = titulo;
        this.anyoLanzamineto = anyoLanzamineto;
        this.duracion = duracion;
        this.guion = guion;
        this.portada = portada;
        this.tipo = tipo;
        this.web = web;
        this.numVisitas = 0;
    }
    public Produccion(int id, String titulo, LocalDate anyoLanzamineto,
                      int duracion, String guion, String portada, Tipo tipo, String web,int numVisitas) {
        this.id = id;
        this.titulo = titulo;
        this.anyoLanzamineto = anyoLanzamineto;
        this.duracion = duracion;
        this.guion = guion;
        this.portada = portada;
        this.tipo = tipo;
        this.web = web;
        this.numVisitas = numVisitas;
    }

    public Produccion(int id, String titulo, Calificacion calificacion, LocalDate anyoLanzamineto, int duracion, ArrayList<Genero> genero,
                      ArrayList<Director> director, ArrayList<Actor> actor, String guion, ArrayList<Idioma> idioma,
                      String portada, Tipo tipo, Productora productora, String web, ArrayList<Plataforma> plataforma) {
        this.id = id;
        this.titulo = titulo;
        this.calificacion = calificacion;
        this.anyoLanzamineto = anyoLanzamineto;
        this.duracion = duracion;
        this.genero = genero;
        this.director = director;
        this.actor = actor;
        this.guion = guion;
        this.idioma = idioma;
        this.portada = portada;
        this.tipo = tipo;
        this.productora = productora;
        this.web = web;
        this.plataforma = plataforma;
        this.numVisitas = 0;
    }

    public int getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public Calificacion getCalificacion() {
        return calificacion;
    }

    public LocalDate getAnyoLanzamineto() {
        return anyoLanzamineto;
    }

    public int getDuracion() {
        return duracion;
    }

    public ArrayList<Genero> getGenero() {
        return genero;
    }

    public ArrayList<Director> getDirector() {
        return director;
    }

    public ArrayList<String> getEscritor() {
        return escritor;
    }

    public ArrayList<Actor> getActor() {
        return actor;
    }

    public String getGuion() {
        return guion;
    }

    public ArrayList<Idioma> getIdioma() {
        return idioma;
    }

    public String getPortada() {
        return portada;
    }

    public Productora getProductora() {
        return productora;
    }

    public String getWeb() {
        return web;
    }

    public ArrayList<Plataforma> getPlataforma() {
        return plataforma;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public int getNumVisitas() {
        return numVisitas;
    }

    public void setGenero(ArrayList<Genero> genero) {
        this.genero = genero;
    }

    public void setDirector(ArrayList<Director> director) {
        this.director = director;
    }

    public void setEscritor(ArrayList<String> escritor) {
        this.escritor = escritor;
    }

    public void setActor(ArrayList<Actor> actor) {
        this.actor = actor;
    }

    public void setIdioma(ArrayList<Idioma> idioma) {
        this.idioma = idioma;
    }

    public void setProductora(Productora productora) {
        this.productora = productora;
    }

    public void setCalificacion(Calificacion calificacion) {
        this.calificacion = calificacion;
    }


    public void setPlataforma(ArrayList<Plataforma> plataforma) {
        this.plataforma = plataforma;
    }

    public void setNumVisitas() {
        this.numVisitas += 1;
    }
}
