package es.segui.modelo.entidades.produccion;

import es.segui.modelo.entidades.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class Pelicula extends Produccion {
    public Pelicula(int id, String titulo, LocalDate anyoLanzamineto, int duracion, String guion, String portada,
                    Tipo tipo, String web, int numVisitas) {
        super(id, titulo, anyoLanzamineto, duracion, guion, portada, tipo, web, numVisitas);
    }

    public Pelicula(int id, String titulo, Calificacion calificacion, LocalDate anyoLanzamineto, int duracion,
                    ArrayList<Genero> genero, ArrayList<Director> director, ArrayList<Actor> actor, String guion,
                    ArrayList<Idioma> idioma,
                    String portada, Tipo tipo , Productora productora, String web, ArrayList<Plataforma> plataforma) {
        super(id, titulo, calificacion, anyoLanzamineto, duracion, genero, director, actor, guion, idioma, portada,
                tipo,productora, web, plataforma);

    }

    public Pelicula(int id, String titulo, LocalDate anyoLanzamineto,
                    int duracion, String guion, String portada, Tipo tipo, String web) {
        super(id, titulo, anyoLanzamineto, duracion, guion, portada, tipo, web);
    }

}
