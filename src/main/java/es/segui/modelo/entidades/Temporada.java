package es.segui.modelo.entidades;

import es.segui.modelo.entidades.produccion.Produccion;

import java.time.LocalDate;
import java.time.Year;

public class Temporada {

    private Produccion produccion;

    private int idTemporada;

    private int anyoLanzamiento;

    private String guion;

    private int numCapitulos;

    public Temporada(Produccion produccion, int idTemporada, int anyoLanzamiento, String guion, int numCapitulos) {
        this.produccion = produccion;
        this.idTemporada = idTemporada;
        this.anyoLanzamiento = anyoLanzamiento;
        this.guion = guion;
        this.numCapitulos = numCapitulos;
    }

    public Produccion getProduccion() {
        return produccion;
    }

    public int getIdTemporada() {
        return idTemporada;
    }

    public int getAnyoLanzamiento() {
        return anyoLanzamiento;
    }

    public String getGuion() {
        return guion;
    }

    public int getNumCapitulos() {
        return numCapitulos;
    }
}
