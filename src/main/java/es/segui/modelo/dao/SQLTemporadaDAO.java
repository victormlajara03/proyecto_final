package es.segui.modelo.dao;

import es.segui.modelo.dao.core.SQLGenericDAO;
import es.segui.modelo.entidades.Genero;
import es.segui.modelo.entidades.Temporada;
import es.segui.modelo.entidades.Tipo;
import es.segui.modelo.entidades.produccion.Pelicula;
import es.segui.modelo.entidades.produccion.Produccion;
import es.segui.modelo.entidades.produccion.Serie;
import es.segui.utils.MySQLConnection;

import java.sql.*;
import java.time.LocalDate;

public class SQLTemporadaDAO  extends SQLGenericDAO {

    private final String PRINCIPAL_TABLE_NAME = "TEMPORADA";

    public void insert(Temporada temporada) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (id, id_temporada, fecha_lanzamiento, plot, capitulos)" +
                " VALUES (?,?,?,?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1, temporada.getProduccion().getId());
            preparedStatement.setInt(2,temporada.getIdTemporada());
            LocalDate fechaLanzamiento = LocalDate.of(temporada.getAnyoLanzamiento(),1,1);
            preparedStatement.setDate(3, Date.valueOf(fechaLanzamiento));
            preparedStatement.setString(4, temporada.getGuion());
            preparedStatement.setInt(5,temporada.getNumCapitulos());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getTable() {
        return PRINCIPAL_TABLE_NAME;
    }

    @Override
    protected Object getFromResultSet(ResultSet rs) throws SQLException {
        SQLProduccionDAO sqlProduccionDAO = new SQLProduccionDAO();
        int idProduccion = rs.getInt("id");
        Produccion produccion = sqlProduccionDAO.findBy("id",idProduccion);
        int idTemporada = rs.getInt("id_temporada");
        LocalDate anyoLanzamiento = rs.getDate("fecha_lanzamiento").toLocalDate();
        String guion = rs.getString("plot");
        int numerocapitulos = rs.getInt("capitulos");
        return new Temporada(produccion,idTemporada,anyoLanzamiento.getYear(),guion,numerocapitulos);
    }
}
