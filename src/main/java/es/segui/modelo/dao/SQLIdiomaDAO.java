package es.segui.modelo.dao;
import es.segui.modelo.entidades.Idioma;
import es.segui.utils.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SQLIdiomaDAO {

    private final String PRINCIPAL_TABLE_NAME = "IDIOMA";

    private final String RELATION_TABLE_NAME = "PRODUCCION_IDIOMA";

    public Idioma findByCod(String cod) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE cod = ?";
        Connection connection = MySQLConnection.getConnection();
        Idioma idioma = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1,cod);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                idioma = getFromResultSet(resultSet);
                return idioma;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idioma;
    }

    public void insert(Idioma idioma) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (id,cod,nombre) VALUES (?,?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1, idioma.getId());
            preparedStatement.setString(2,idioma.getCod());
            preparedStatement.setString(3, idioma.getDescripcion());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertRelation(int codPelicula,int codIdioma) {
        String sql = "INSERT INTO " + RELATION_TABLE_NAME + " (id_idioma,id_produccion) VALUES (?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1,codIdioma);
            preparedStatement.setInt(2,codPelicula);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Idioma> findByIdProd(int id){
        ArrayList<Idioma> idiomas = new ArrayList<>();
        String sql = "SELECT * FROM IDIOMA i   INNER JOIN PRODUCCION_IDIOMA p ON i.id = p.id_idioma " +
                "WHERE p.id_produccion = ?";
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                idiomas.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idiomas;
    }

    private Idioma getFromResultSet(ResultSet rs) throws SQLException{
        int id = rs.getInt("id");
        String cod = rs.getString("cod");
        String nombre = rs.getString("nombre");
        return new Idioma(id,cod,nombre);
    }
}
