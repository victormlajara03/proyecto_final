package es.segui.modelo.dao;
import es.segui.utils.MySQLConnection;
import java.sql.*;
import java.util.HashMap;


public class SQLValorarDAO {

    private final String PRINCIPAL_TABLE_NAME = "VALORAR";

    private boolean estarValorada(String email, int id) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE correo = ? AND id_produccion = ?";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, email);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
               return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void save(String email, int id, int valoracion) {
        if (estarValorada(email,id)) {
            update(email,id,valoracion);
        } else {
            insert(email,id,valoracion);
        }
    }

    private void insert(String email, int id, int valoracion) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (correo,id_produccion,puntuacion)" +
                " VALUES (?,?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, email);
            preparedStatement.setInt(2, id);
            preparedStatement.setInt(3, valoracion);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void update(String email, int id, int valoracion) {
        String sql = "UPDATE " + PRINCIPAL_TABLE_NAME + " SET puntuacion = ? WHERE correo " +
                "= ? AND id_produccion = ?";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1, valoracion);
            preparedStatement.setString(2,email);
            preparedStatement.setInt(3,id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public HashMap<Integer,Float> obtenerPuntuacionMedia(){
        String sql = "SELECT v.id_produccion ,AVG(v.puntuacion) as media FROM  VALORAR v  GROUP BY v.id_produccion ";
        HashMap<Integer, Float> putuaciones = new HashMap<>();
        Connection connection = MySQLConnection.getConnection();
        try(Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                int idFilm = Integer.parseInt(resultSet.getString("id_produccion"));
                float puntuacion = Float.parseFloat(resultSet.getString("media"));
                putuaciones.put(idFilm,puntuacion);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return putuaciones;
    }
}
