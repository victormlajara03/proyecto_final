package es.segui.modelo.dao;

import es.segui.modelo.entidades.Productora;
import es.segui.modelo.entidades.produccion.Produccion;
import es.segui.utils.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLProductoraDAO {

    private final String PRINCIPAL_TABLE_NAME = "PRODUCTORA";

    public Productora findByName(String name) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE nombre = ?";
        Connection connection = MySQLConnection.getConnection();
        Productora productora = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                productora = getFromResultSet(resultSet);
                return productora;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productora;
    }

    public Productora findById(int id) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE id = ?";
        Connection connection = MySQLConnection.getConnection();
        Productora productora = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                productora = getFromResultSet(resultSet);
                return productora;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productora;
    }

    public void insert(Productora productora) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (nombre) VALUES (?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, productora.getNombre());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                int autoIncremental = rs.getInt(1);
                productora.setCod(autoIncremental);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Productora findByIdProd(int id){
        Productora productora = null;
        String sql = "SELECT p.* FROM PRODUCTORA p inner JOIN PRODUCCION  ON PRODUCCION.productora = p.id " +
                "            WHERE PRODUCCION.id = ?";
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                productora = getFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productora;
    }



    private Productora getFromResultSet(ResultSet rs) throws SQLException{
        int cod = rs.getInt("id");
        String nombre = rs.getString("nombre");
        return new Productora(cod,nombre);
    }
}
