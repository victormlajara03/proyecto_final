package es.segui.modelo.dao;

import es.segui.modelo.entidades.Plataforma;
import es.segui.utils.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SQLPlataformaDAO {

    private final String PRINCIPAL_TABLE_NAME = "PLATAFORMA";

    private final String RELATION_TABLE_NAME = "PRODUCCION_PLATAFORMA";


    public Plataforma findByName(String name) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE nombre = ?";
        Connection connection = MySQLConnection.getConnection();
        Plataforma plataforma = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                plataforma = getFromResultSet(resultSet);
                return plataforma;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return plataforma;
    }

    public void insert(Plataforma plataforma) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (nombre) VALUES (?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, plataforma.getNombre());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                int autoIncremental = rs.getInt(1);
                plataforma.setCod(autoIncremental);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertRelation(int codPelicula,int codPlataforma) {
        String sql = "INSERT INTO " + RELATION_TABLE_NAME + " (id_plataforma,id_produccion) VALUES (?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1,codPlataforma);
            preparedStatement.setInt(2,codPelicula);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Plataforma> findByIdProd(int id){
        ArrayList<Plataforma> plataformas = new ArrayList<>();
        String sql = "SELECT p.* FROM PLATAFORMA p JOIN PRODUCCION_PLATAFORMA pp  " +
                "ON p.id = pp.id_plataforma WHERE pp.id_produccion = ? ";
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                plataformas.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return plataformas;
    }

    private Plataforma getFromResultSet(ResultSet rs) throws SQLException{
        int cod = rs.getInt("id");
        String nombre = rs.getString("nombre");
        return new Plataforma(cod,nombre);
    }
}
