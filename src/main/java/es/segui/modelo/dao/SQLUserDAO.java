package es.segui.modelo.dao;

import es.segui.modelo.dao.core.SQLGenericDAO;
import es.segui.modelo.entidades.Actor;
import es.segui.modelo.entidades.Usuario;
import es.segui.utils.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLUserDAO extends SQLGenericDAO<Usuario> {

    private final String PRINCIPAL_TABLE_NAME = "USUARIO";

    public void insert(Usuario usuario) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (correo,nombre,password,tipo) VALUES (?,?,?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, usuario.getEmail());
            preparedStatement.setString(2, usuario.getNombre());
            preparedStatement.setString(3, usuario.getPassword());
            preparedStatement.setInt(4,usuario.getTipo());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Usuario findBy(String key, String value) {
        return super.findBy(key, value);
    }

    @Override
    protected String getTable() {
        return PRINCIPAL_TABLE_NAME;
    }

    @Override
    protected Usuario getFromResultSet(ResultSet resultSet) throws SQLException {
        String correo = resultSet.getString("correo");
        String nombre = resultSet.getString("nombre");
        String password = resultSet.getString("password");
        int tipo = resultSet.getInt("tipo");
        return new Usuario(correo,nombre,password,tipo);
    }
}
