package es.segui.modelo.dao;

import es.segui.modelo.entidades.Actor;
import es.segui.modelo.entidades.Genero;
import es.segui.utils.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SQLActorDAO {

    private final String PRINCIPAL_TABLE_NAME = "ACTOR";

    private final String RELATION_TABLE_NAME = "PRODUCCION_ACTOR";

    public Actor findByName(String name) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE nombre = ?";
        Connection connection = MySQLConnection.getConnection();
        Actor actor = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                actor = getFromResultSet(resultSet);
                return actor;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actor;
    }

    public void insertActor(Actor actor) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (nombre) VALUES (?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, actor.getName());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                int autoIncremental = rs.getInt(1);
                actor.setCod(autoIncremental);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertRelation(int codPelicula,int codActor) {
        String sql = "INSERT INTO " + RELATION_TABLE_NAME + " (id_actor,id_produccion) VALUES (?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1,codActor);
            preparedStatement.setInt(2,codPelicula);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Actor> findByIdProd(int id){
        ArrayList<Actor> actors = new ArrayList<>();
        String sql = "SELECT a.* FROM ACTOR a LEFT JOIN PRODUCCION_ACTOR p  ON  a.id = p.id_actor " +
                "WHERE p.id_produccion = ? ;";
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                actors.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actors;
    }

    private Actor getFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String nombre = rs.getString("nombre");
        return new Actor(id,nombre);
    }


}
