package es.segui.modelo.dao;

import es.segui.modelo.entidades.Calificacion;
import es.segui.modelo.entidades.Productora;
import es.segui.utils.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLCalificacionDAO {

    private final String PRINCIPAL_TABLE_NAME = "CALIFICACION";

    public Calificacion findByName(String calificacion) {

        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE calificacion = ?";
        Connection connection = MySQLConnection.getConnection();
        Calificacion calificacion1 = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1,calificacion);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                calificacion1 = getFromResultSet(resultSet);
                return calificacion1;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return calificacion1;
    }

    public Calificacion findById(int id) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE id = ?";
        Connection connection = MySQLConnection.getConnection();
        Calificacion calificacion = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                calificacion = getFromResultSet(resultSet);
                return calificacion;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return calificacion;
    }

    public void insert(Calificacion calificacion) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (calificacion) VALUES (?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, calificacion.getNombre());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                int autoIncremental = rs.getInt(1);
                calificacion.setCod(autoIncremental);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Calificacion findByIdProd(int id){
        Calificacion calificacion = null;
        String sql = "SELECT p.* FROM CALIFICACION  p inner JOIN PRODUCCION  ON PRODUCCION.calificacion = p.id " +
                "WHERE PRODUCCION.id = ?";
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                calificacion = getFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return calificacion;
    }

    private Calificacion getFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String calificacion = rs.getString("calificacion");
        return new Calificacion(id,calificacion);
    }


}
