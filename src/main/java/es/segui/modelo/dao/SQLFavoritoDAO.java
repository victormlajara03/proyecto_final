package es.segui.modelo.dao;

import es.segui.utils.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SQLFavoritoDAO {

    private final String PRINCIPAL_TABLE_NAME = "FAVORITO";

    private boolean esFavorito(String email, int id) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE correo = ? AND id_produccion = ?";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, email);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void save(String email, int id) {
        if (esFavorito(email,id)) {
            delete(email,id);
        } else {
            insert(email,id);
        }
    }

    private void insert(String email, int id) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (correo,id_produccion)" +
                " VALUES (?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, email);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void delete(String email, int id) {
        String sql = "DELETE FROM " + PRINCIPAL_TABLE_NAME + " WHERE correo = ? AND id_produccion = ?";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, email);
            preparedStatement.setInt(2,id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Integer> findIdProdFav(String correo) {
        String sql = "SELECT id_produccion FROM " + PRINCIPAL_TABLE_NAME + " WHERE correo = ?";
        Connection connection = MySQLConnection.getConnection();
        ArrayList<Integer> lista = new ArrayList<>();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, correo);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                lista.add(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

    public int countFav(String correo) {
        String sql = "SELECT COUNT(*) AS cantidad  FROM " + PRINCIPAL_TABLE_NAME + " WHERE correo = ?";
        Connection connection = MySQLConnection.getConnection();
        int cantidad = 0;
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, correo);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                cantidad = rs.getInt("cantidad");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cantidad;
    }

}
