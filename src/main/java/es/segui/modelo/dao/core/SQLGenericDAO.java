package es.segui.modelo.dao.core;

import es.segui.utils.MySQLConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

public abstract class SQLGenericDAO<Datos> {

    private final String SQL_FIND_ALL = "SELECT * FROM " + getTable();

    private final String SQL_COUNT_ALL = "SELECT COUNT(*) AS cantidad FROM " + getTable();

    private final String SQL_ORDER_BY = " ORDER BY flanzamiento ";

    public ArrayList<Datos> findAll(){
        ArrayList<Datos> list = new ArrayList<>();
        String sql = "SELECT * FROM " + getTable();
        Connection connection = getConnection();
        try(Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Datos dato = getFromResultSet(resultSet);
                list.add(dato);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Datos> findAllBy(String key,String value){
        ArrayList<Datos> list = new ArrayList<>();
        String sql = "SELECT * FROM "+ getTable()+" WHERE " + key + " = ?";
        obtenerLista(list,value,sql);
        return list;
    }


    public ArrayList<Datos> findAllByLike(String key,String value){
        ArrayList<Datos> list = new ArrayList<>();
        String sql = "SELECT * FROM "+ getTable()+" WHERE " + key + " like  ?";
        value = "%"+value+"%";
        obtenerLista(list,value,sql);
        return list;
    }

    private void obtenerLista(ArrayList<Datos> list,String value,String sql){
        Connection connection = getConnection();
        try(PreparedStatement preparedStatement =
                    connection.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS)){
            preparedStatement.setString(1,value);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Datos dato = getFromResultSet(resultSet);
                list.add(dato);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public Datos findBy(String key, int value){
       return findBy(key, String.valueOf(value));
    }

    public Datos findBy(String key, String value){
        String sql = "SELECT * FROM " + getTable() + " WHERE " + key + " = ? LIMIT 1;";
        Connection connection = getConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS)){
            preparedStatement.setString(1,value);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                return getFromResultSet(resultSet);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }


    protected abstract String getTable();


    protected abstract Datos getFromResultSet(ResultSet resultSet) throws SQLException;

    protected Connection getConnection(){
        return MySQLConnection.getConnection();
    }

    public ArrayList<Datos> findAllWithParams(Map<String, String> params, Integer limit, Integer offset,String orden) {
        StringBuilder sqlWHERE = new StringBuilder();
        params.keySet().forEach((key) -> {
            if (sqlWHERE.length() == 0) {
                sqlWHERE.append(" WHERE ").append(key).append(" = ? ");
            } else {
                sqlWHERE.append("AND ").append(key).append(" = ? ");
            }
        });
        sqlWHERE.append(SQL_ORDER_BY).append(orden);
        if (offset != null || limit != null) {
            sqlWHERE.append(" LIMIT ");
            if (offset != null) {
                sqlWHERE.append(offset).append(",");
            }
            sqlWHERE.append(limit);
        }
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL + sqlWHERE  ))
        {
            int paramNumber = 1;
            for (String key: params.keySet()) {
                statement.setString(paramNumber, params.get(key));
            }
            return getMultipleResults(statement);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return new ArrayList<>();
    }

    public ArrayList<Datos> findAllWithParamsUsenLike(Map<String, String> params, Integer limit, Integer offset,String orden) {
        StringBuilder sqlWHERE = new StringBuilder();
        params.keySet().forEach((key) -> {
            if (sqlWHERE.length() == 0) {
                sqlWHERE.append(" WHERE ").append(key).append(" like ? ");
            } else {
                sqlWHERE.append("AND ").append(key).append(" like ? ");
            }
        });
        sqlWHERE.append(SQL_ORDER_BY).append(orden);
        if (offset != null || limit != null) {
            sqlWHERE.append(" LIMIT ");
            if (offset != null) {
                sqlWHERE.append(offset).append(",");
            }
            sqlWHERE.append(limit);
        }
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL + sqlWHERE ))
        {
            int paramNumber = 1;
            for (String key: params.keySet()) {
                statement.setString(paramNumber, "%" +params.get(key)+"%");
            }
            return getMultipleResults(statement);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return new ArrayList<>();
    }

    public int cantidadBuscada(Map<String, String> params){
        StringBuilder sqlWHERE = new StringBuilder();
        int cantidad = 0;
        params.keySet().forEach((key) -> {
            if (sqlWHERE.length() == 0) {
                sqlWHERE.append(" WHERE ").append(key).append(" like ? ");
            } else {
                sqlWHERE.append("AND ").append(key).append(" like ? ");
            }
        });
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_COUNT_ALL + sqlWHERE))
        {
            int paramNumber = 1;
            for (String key: params.keySet()) {
                statement.setString(paramNumber, params.get(key));
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                cantidad = resultSet.getInt("cantidad");
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return cantidad;
    }

    protected ArrayList<Datos> getMultipleResults(PreparedStatement statement) throws SQLException {
        ArrayList<Datos> listado = new ArrayList<>();
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            Datos entidad = getFromResultSet(resultSet);
            listado.add(entidad);
        }
        return listado;
    }

}
