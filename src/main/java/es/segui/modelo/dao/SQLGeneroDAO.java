package es.segui.modelo.dao;

import es.segui.modelo.dao.core.SQLGenericDAO;
import es.segui.modelo.entidades.Genero;
import es.segui.utils.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SQLGeneroDAO extends SQLGenericDAO<Genero> {

    private final String PRINCIPAL_TABLE_NAME = "GENERO";

    private final String RELATION_TABLE_NAME = "PRODUCCION_GENERO";

    public Genero findByCod(String cod) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE cod = ?";
        Connection connection = MySQLConnection.getConnection();
        Genero genero = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1,cod);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                genero = getFromResultSet(resultSet);
                return genero;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genero;
    }

    public void insert(Genero genero) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (id,cod,descripcion) VALUES (?,?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1, genero.getId());
            preparedStatement.setString(2,genero.getCod());
            preparedStatement.setString(3, genero.getDescripcion());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertRelation(int codPelicula,int codGenero) {
        String sql = "INSERT INTO " + RELATION_TABLE_NAME + " (id_genero,id_produccion) VALUES (?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1,codGenero);
            preparedStatement.setInt(2,codPelicula);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Genero> findByIdProd(int id){
        ArrayList<Genero> generos = new ArrayList<>();
        String sql = "SELECT GENERO.* FROM GENERO LEFT JOIN PRODUCCION_GENERO ON PRODUCCION_GENERO.id_genero" +
                " = GENERO.id WHERE PRODUCCION_GENERO.id_produccion = ? ";
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                generos.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return generos;
    }

    @Override
    protected Genero getFromResultSet(ResultSet rs) throws SQLException{
        int id = rs.getInt("id");
        String cod = rs.getString("cod");
        String descripcion = rs.getString("descripcion");
        return new Genero(id,cod,descripcion);
    }

    @Override
    protected String getTable() {
        return PRINCIPAL_TABLE_NAME;
    }


}
