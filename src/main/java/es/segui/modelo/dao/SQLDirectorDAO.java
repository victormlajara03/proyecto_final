package es.segui.modelo.dao;

import es.segui.modelo.entidades.Director;
import es.segui.utils.MySQLConnection;

import java.sql.*;
import java.util.ArrayList;

public class SQLDirectorDAO {

    private final String PRINCIPAL_TABLE_NAME = "DIRECTOR";

    private final String RELATION_TABLE_NAME = "PRODUCCION_DIRECTOR";

    public Director findByName(String name) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE nombre = ?";
        Connection connection = MySQLConnection.getConnection();
        Director director = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                director = getFromResultSet(resultSet);
                return director;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return director;
    }

    public void insert(Director director) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (nombre) VALUES (?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setString(1, director.getNombre());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                int autoIncremental = rs.getInt(1);
                director.setCod(autoIncremental);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertRelation(int codPelicula,int codDirector) {
        String sql = "INSERT INTO " + RELATION_TABLE_NAME + " (id_director,id_produccion) VALUES (?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1,codDirector);
            preparedStatement.setInt(2,codPelicula);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Director> findByIdProd(int id){
        ArrayList<Director> directors = new ArrayList<>();
        String sql = "SELECT * FROM DIRECTOR d  LEFT JOIN PRODUCCION_DIRECTOR pd ON d.id = pd.id_director" +
                " WHERE pd.id_produccion = ?;";
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                directors.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return directors;
    }

    private Director getFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String nombre = rs.getString("nombre");
        return new Director(id,nombre);
    }
}
