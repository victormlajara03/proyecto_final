package es.segui.modelo.dao;

import es.segui.modelo.dao.core.SQLGenericDAO;
import es.segui.modelo.entidades.*;
import es.segui.modelo.entidades.produccion.Pelicula;
import es.segui.modelo.entidades.produccion.Produccion;
import es.segui.modelo.entidades.produccion.Serie;
import es.segui.utils.MySQLConnection;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;

public class SQLProduccionDAO extends SQLGenericDAO<Produccion> {

    private final String PRINCIPAL_TABLE_NAME = "PRODUCCION";

    private final String RELATION_GENDER_NAME = "PRODUCCION_GENERO";

    public void insertProduccion(Produccion produccion) {
        String sql = "INSERT INTO " + PRINCIPAL_TABLE_NAME + " (id,titulo,calificacion,flanzamiento,duracion,guion" +
                ",portada,tipo,productora,url_trailer, numVisitas) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1, produccion.getId());
            preparedStatement.setString(2, produccion.getTitulo());
            preparedStatement.setInt(3, produccion.getCalificacion().getCod());
            preparedStatement.setDate(4, Date.valueOf(produccion.getAnyoLanzamineto()));
            preparedStatement.setInt(5, produccion.getDuracion());
            preparedStatement.setString(6, produccion.getGuion());
            preparedStatement.setString(7, produccion.getPortada());
            preparedStatement.setString(8, String.valueOf(produccion.getTipo()));
            preparedStatement.setInt(9,produccion.getProductora().getCod());
            preparedStatement.setString(10, produccion.getWeb());
            preparedStatement.setInt(11,produccion.getNumVisitas());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Produccion findById(int id) {
        String sql = "SELECT * FROM " + PRINCIPAL_TABLE_NAME + " WHERE id = ?";
        Connection connection = MySQLConnection.getConnection();
        Produccion produccion = null;
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                produccion = getFromResultSet(resultSet);
                return produccion;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return produccion;
    }

    public void updateVisitas(Produccion produccion) {
        String sql = "UPDATE " + PRINCIPAL_TABLE_NAME + " SET numVisitas = ? WHERE id = ?";
        Connection connection = MySQLConnection.getConnection();
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ){
            preparedStatement.setInt(1, produccion.getNumVisitas());
            preparedStatement.setInt(2, produccion.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Produccion getFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String titulo = rs.getString("titulo");
        LocalDate anyoLanzamiento = rs.getDate("flanzamiento").toLocalDate();
        int duracion = rs.getInt("duracion");
        String guion = rs.getString("guion");
        String portada = rs.getString("portada");
        Tipo tipo = Tipo.fromString(rs.getString("tipo"));
        String url = rs.getString("url_trailer");
        int numVisitas = rs.getInt("numVisitas");
        if (tipo == Tipo.MOVIE) {
            return new Pelicula(id,titulo,anyoLanzamiento,duracion,guion,portada,tipo,url,numVisitas);
        } else {
            return new Serie(id,titulo,anyoLanzamiento,duracion,guion,portada,tipo,url,numVisitas);
        }
    }

    public ArrayList<Produccion> findTop(String tipo){
        String sql = "SELECT p.* FROM " + PRINCIPAL_TABLE_NAME + " p LEFT JOIN VALORAR  ON p.id = " +
                "VALORAR.id_produccion WHERE tipo = ? GROUP BY p.id Order by AVG(VALORAR.puntuacion) DESC Limit 5";
        ArrayList<Produccion> lista = new ArrayList<>();
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1,tipo);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lista.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

    public ArrayList<Produccion> findByTituloAndGener(String titulo,String codGenero,Integer limit, Integer offset ,
                                                      String orden){
        StringBuilder sql= new StringBuilder();
        sql.append("SELECT p.* FROM PRODUCCION p LEFT JOIN PRODUCCION_GENERO pg on p.id = pg.id_produccion WHERE titulo " +
                "like ? AND pg.id_genero = ? GROUP BY p.id ORDER BY flanzamiento  "
                + orden + " ");
        if (offset != null || limit != null) {
            sql.append("LIMIT ");
            if (offset != null) {
                sql.append(offset).append(",");
            }
            sql.append(limit);
        }
        ArrayList<Produccion> lista = new ArrayList<>();
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql.toString())
        ) {
            preparedStatement.setString(1,"%" + titulo + "%");
            preparedStatement.setString(2,codGenero);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lista.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lista;
    }

    public ArrayList<Produccion> findByTituloAndGener(String titulo,Integer limit, Integer offset , String orden){
        StringBuilder sql= new StringBuilder();
        sql.append("SELECT p.* FROM PRODUCCION p LEFT JOIN PRODUCCION_GENERO pg on p.id = pg.id_produccion WHERE titulo " +
                "like ? GROUP BY p.id ORDER BY flanzamiento "
                + orden + " ");
        if (offset != null || limit != null) {
            sql.append("LIMIT ");
            if (offset != null) {
                sql.append(offset).append(",");
            }
            sql.append(limit);
        }
        ArrayList<Produccion> lista = new ArrayList<>();
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql.toString())
        ) {
            preparedStatement.setString(1,"%" + titulo + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lista.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lista;
    }

    public int countFindByTituloAndGener(String titulo,String codGenero){
        StringBuilder sql= new StringBuilder();
        if (codGenero.equals("")){
            codGenero = "%";
        }
        sql.append("SELECT COUNT(*) AS cantidad FROM " + PRINCIPAL_TABLE_NAME + " WHERE titulo like ? AND id IN " +
                "(SELECT id_produccion  FROM "+ RELATION_GENDER_NAME + " WHERE id_genero like ?) ");
        int cantidad = 0;
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql.toString())
        ) {
            preparedStatement.setString(1,"%" + titulo + "%");
            preparedStatement.setString(2,codGenero);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                cantidad = resultSet.getInt("cantidad");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return cantidad;
    }

    @Override
    protected String getTable() {
        return PRINCIPAL_TABLE_NAME;
    }

    public void completeProduction(Produccion produccion){
        findByIdPopulateGenero(produccion);
        findByIdPopulateArctor(produccion);
        findByIdPopulateDirectors(produccion);
        findByIdPopulateLenguages(produccion);
        findByIdPopulateProductora(produccion);
        findByIdPopulateCalificacion(produccion);
        findByIdPopulatePlataforma(produccion);
    }

    private void findByIdPopulateGenero(Produccion produccion){
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        ArrayList<Genero> generos = sqlGeneroDAO.findByIdProd(produccion.getId());
        produccion.setGenero(generos);
    }

    private void findByIdPopulateArctor(Produccion produccion){
        SQLActorDAO sqlActorDAO = new SQLActorDAO();
        ArrayList<Actor> actors = sqlActorDAO.findByIdProd(produccion.getId());
        produccion.setActor(actors);
    }

    private void findByIdPopulateDirectors(Produccion produccion){
        SQLDirectorDAO sqlDirectorDAO = new SQLDirectorDAO();
        ArrayList<Director> directors = sqlDirectorDAO.findByIdProd(produccion.getId());
        produccion.setDirector(directors);
    }

    private void findByIdPopulateLenguages(Produccion produccion){
        SQLIdiomaDAO sqlIdiomaDAO = new SQLIdiomaDAO();
        ArrayList<Idioma> directors = sqlIdiomaDAO.findByIdProd(produccion.getId());
        produccion.setIdioma(directors);
    }

    private void findByIdPopulateProductora(Produccion produccion){
        SQLProductoraDAO sqlProductoraDAO = new SQLProductoraDAO();
        Productora productora = sqlProductoraDAO.findByIdProd(produccion.getId());
        produccion.setProductora(productora);
    }

    private void findByIdPopulateCalificacion(Produccion produccion){
        SQLCalificacionDAO sqlProductoraDAO = new SQLCalificacionDAO();
        Calificacion productora = sqlProductoraDAO.findByIdProd(produccion.getId());
        produccion.setCalificacion(productora);
    }

    private void findByIdPopulatePlataforma(Produccion produccion){
        SQLPlataformaDAO sqlPlataformaDAO = new SQLPlataformaDAO();
        ArrayList<Plataforma> plataformas = sqlPlataformaDAO.findByIdProd(produccion.getId());
        produccion.setPlataforma(plataformas);
    }

    public ArrayList<Produccion> findByFavoritos(Integer limit, Integer offset , String orden, String correo){
        StringBuilder sql= new StringBuilder();
        sql.append("SELECT p.* FROM PRODUCCION  p LEFT JOIN FAVORITO f ON f.id_produccion = p.id WHERE f.correo = ? " +
                " ORDER BY flanzamiento "
                + orden + " ");
        if (offset != null || limit != null) {
            sql.append("LIMIT ");
            if (offset != null) {
                sql.append(offset).append(",");
            }
            sql.append(limit);
        }
        ArrayList<Produccion> lista = new ArrayList<>();
        Connection connection = MySQLConnection.getConnection();
        try(
                PreparedStatement preparedStatement = connection.prepareStatement(sql.toString())
        ) {
            preparedStatement.setString(1,correo);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lista.add(getFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lista;
    }



}
