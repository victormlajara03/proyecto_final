package es.segui.utils;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySQLConnection {

    private static Connection connection;

    private static Properties dbProperties;

    static {
        readDatabaseProperties();
    }

    /**
     * Read Database Properties from configuration file
     */
    private static void readDatabaseProperties() {
        try {
            Properties properties= new Properties();
            String filePath = MySQLConnection.class.getClassLoader().getResource("db.config.properties").getFile();
            properties.load(new FileReader(filePath));
            dbProperties = properties;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Singleton Connection implementation
     *
     * @return Connection
     */
    public static Connection getConnection() {
        if (connection == null) {
            registeDriver();
            try {
                String dbURL = "jdbc:mysql://" + dbProperties.getProperty("host") + "/" + dbProperties.getProperty("database") + "?serverTimezone=UTC&allowPublicKeyRetrieval=true";
                connection = DriverManager.getConnection(dbURL, dbProperties.getProperty("user"), dbProperties.getProperty("password"));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    private static void registeDriver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error al registrar el driver de MySQL: " + ex);
        }
    }

}
