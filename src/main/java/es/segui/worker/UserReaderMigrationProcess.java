package es.segui.worker;

import es.segui.modelo.dao.SQLProduccionDAO;
import es.segui.modelo.dao.SQLUserDAO;
import es.segui.modelo.entidades.Usuario;

import java.util.ArrayList;

public class UserReaderMigrationProcess {

    public static void main(String[] args) {

        CSVUserReader csvUserReader = new CSVUserReader();
        ArrayList<Usuario> usuarios = csvUserReader.findAll();
        SQLUserDAO sqlUserDAO = new SQLUserDAO();
        for (int i = 0; i < usuarios.size(); i++) {
            sqlUserDAO.insert(usuarios.get(i));
        }
    }


}
