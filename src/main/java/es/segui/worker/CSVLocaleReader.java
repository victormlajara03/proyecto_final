package es.segui.worker;

import es.segui.modelo.entidades.Idioma;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CSVLocaleReader {

    private final String DATABASE_FILE = "locales.csv";

    private File fileDatabase;

    public CSVLocaleReader() {
        ClassLoader classLoader = getClass().getClassLoader();
        fileDatabase = new File(classLoader.getResource(DATABASE_FILE).getFile());
    }

    public ArrayList<Idioma> findAll() {
        ArrayList<Idioma> idiomas = new ArrayList<>();
        boolean firstLine = true;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileDatabase))){
            do {
                String localeRegister = bufferedReader.readLine();
                if (localeRegister == null) {
                    return idiomas;
                }
                if (firstLine) {
                    firstLine = false;
                } else {
                    Idioma idioma = mapToLocale(localeRegister);
                    idiomas.add(idioma);
                }
            } while (true);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return idiomas;
    }

    private Idioma mapToLocale(String line) {
        String[] datosLocale = line.split(",");
        int id = Integer.parseInt(datosLocale[0].trim());
        String cod = datosLocale[1].trim();
        String descripcion = datosLocale[2].trim();
        return new Idioma(id,cod,descripcion);
    }
}
