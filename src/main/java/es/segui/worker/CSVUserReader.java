package es.segui.worker;

import es.segui.controlador.PasswordHelper;
import es.segui.modelo.entidades.Usuario;
import es.segui.modelo.entidades.produccion.Produccion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CSVUserReader {

    private final String DATABASE_FILE = "usuarios.csv";

    private File fileDatabase;

    public CSVUserReader() {
        ClassLoader classLoader = getClass().getClassLoader();
        fileDatabase = new File(classLoader.getResource(DATABASE_FILE).getFile());
    }

    public ArrayList<Usuario> findAll() {
        ArrayList<Usuario> usuarios = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileDatabase))){
            do {
                String usuariosRegister = bufferedReader.readLine();
                if (usuariosRegister == null) {
                    return usuarios;
                }
                Usuario usuario = mapToUser(usuariosRegister);
                usuarios.add(usuario);

            } while (true);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return usuarios;
    }

    private Usuario mapToUser(String line) {
        String [] datosUsuario = line.split(";");
        String email = datosUsuario[0].trim();
        String nombre = datosUsuario[1].trim();
        String password = PasswordHelper.generateSha1(datosUsuario[2].trim());
        int tipo = Integer.parseInt(datosUsuario[3].trim());
        return new Usuario(email,nombre,password,tipo);


    }

}
