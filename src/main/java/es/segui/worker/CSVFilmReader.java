package es.segui.worker;

import es.segui.modelo.dao.*;
import es.segui.modelo.entidades.*;
import es.segui.modelo.entidades.produccion.Pelicula;
import es.segui.modelo.entidades.produccion.Produccion;
import es.segui.modelo.entidades.produccion.Serie;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

public class CSVFilmReader {

    private final String DATABASE_FILE = "listado_peliculas_series.csv";

    private File fileDatabase;

    public CSVFilmReader() {
        ClassLoader classLoader = getClass().getClassLoader();
        fileDatabase = new File(classLoader.getResource(DATABASE_FILE).getFile());
    }

    public ArrayList<Produccion> findAll() {
        ArrayList<Produccion> producciones = new ArrayList<>();
        boolean firstLine = true;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileDatabase))){
            do {
                String produccionesRegister = bufferedReader.readLine();
                if (produccionesRegister == null) {
                    return producciones;
                }
                if (firstLine) {
                    firstLine = false;
                } else {
                    Produccion produccion = mapToProduccion(produccionesRegister);
                    producciones.add(produccion);
                }
            } while (true);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return producciones;
    }

    private Produccion mapToProduccion(String line) {
        String[] datosPelicula = line.split(";");
        int id = Integer.parseInt(datosPelicula[0].trim());
        String titulo = datosPelicula[1].trim();
        Calificacion calificacion = mapToCalificacion(datosPelicula[2].trim());
        LocalDate fechaLanzamiento = mapToDate(datosPelicula[3].trim());
        int duracion = 0;
        if (!datosPelicula[4].equalsIgnoreCase("N/A")) {
            duracion = mapToInteger(datosPelicula[4].trim());
        }
        ArrayList<Genero> generos = mapToGenero(datosPelicula[5].trim());
        ArrayList<Director> directores = mapToDirectores(datosPelicula[6].trim());
        ArrayList<Actor> actores = mapToActor(datosPelicula[7].trim());
        String guion = datosPelicula[8].trim();
        ArrayList<Idioma> idiomas = mapToIdioma(datosPelicula[9].trim());
        String portada = datosPelicula[10].trim();
        Tipo tipo = Tipo.fromString(datosPelicula[11].trim());
        Productora productora = mapToProductora(datosPelicula[12].trim());
        String web = null;
        if (!datosPelicula[13].equals("N/A")){
            web = datosPelicula[13];
        }
        ArrayList<Plataforma> plataformas = mapToPlataforma(datosPelicula[14].trim());

        if (tipo == Tipo.MOVIE) {
            return new Pelicula(id,titulo,calificacion,fechaLanzamiento,duracion,generos,directores,actores,guion,idiomas,
                    portada,tipo,productora,web,plataformas);

        } else {
            return new Serie(id,titulo,calificacion,fechaLanzamiento,duracion,generos,directores,actores,guion,idiomas,
                    portada,tipo,productora,web,plataformas);

        }
       }

    private Calificacion mapToCalificacion(String calificacion) {
        SQLCalificacionDAO sqlCalificacionDAO = new SQLCalificacionDAO();
        Calificacion calificacion1 = sqlCalificacionDAO.findByName(calificacion);
        if (calificacion1 == null) {
            calificacion1 = new Calificacion(-1,calificacion);
            sqlCalificacionDAO.insert(calificacion1);
        }
        return calificacion1;
    }

    private ArrayList<Director> mapToDirectores(String directores) {
        SQLDirectorDAO sqlDirectorDAO = new SQLDirectorDAO();
        ArrayList<Director> listaDirectores = new ArrayList<>();
        String[] nombresDirectores = directores.split(",");
        for (int i = 0; i < nombresDirectores.length; i++) {
            Director director = sqlDirectorDAO.findByName(nombresDirectores[i].trim());
            if (director == null) {
                director = new Director(-1,nombresDirectores[i].trim());
                sqlDirectorDAO.insert(director);
            }
            listaDirectores.add(director);
        }
        return listaDirectores;
    }

    private LocalDate mapToDate(String date) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);
        return LocalDate.parse(date  , dateTimeFormatter);
    }

    private int mapToInteger(String minutos) {
        String[] datosSeparados = minutos.split(" ");
        return Integer.parseInt(datosSeparados[0].trim());
    }

    private ArrayList<Genero> mapToGenero(String generos) {
        ArrayList<Genero> listaGeneros = new ArrayList<>();
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        String[] datosGenero = generos.split(",");
        for (int i = 0; i < datosGenero.length; i++) {
            Genero genero = sqlGeneroDAO.findByCod(datosGenero[i].trim());
            listaGeneros.add(genero);
        }
        return listaGeneros;
    }

    private ArrayList<Actor> mapToActor(String actores) {
        ArrayList<Actor> actores1 = new ArrayList<>();
        SQLActorDAO sqlActorDAO = new SQLActorDAO();
        String[] listaActores = actores.split(",");
        for (int i = 0; i < listaActores.length; i++) {
            Actor actor = sqlActorDAO.findByName(listaActores[i].trim());
            if (actor == null) {
                actor = new Actor(-1,listaActores[i].trim());
                sqlActorDAO.insertActor(actor);
            }
            actores1.add(actor);
        }
        return actores1;
    }

    private ArrayList<Idioma> mapToIdioma(String idiomas) {
        ArrayList<Idioma> listaIdiomas = new ArrayList<>();
        SQLIdiomaDAO sqlIdiomaDAO = new SQLIdiomaDAO();
        String[] idiomas1 = idiomas.split(",");
        for (int i = 0; i < idiomas1.length; i++) {
            Idioma idioma = sqlIdiomaDAO.findByCod(idiomas1[i].trim());
            listaIdiomas.add(idioma);
        }
        return listaIdiomas;
    }

    private Productora mapToProductora(String productora){
        SQLProductoraDAO sqlProductoraDAO = new SQLProductoraDAO();
        Productora productora1 = sqlProductoraDAO.findByName(productora);
        if (productora1 != null){
            return productora1;
        }
        productora1 = new Productora(-1,productora);
        sqlProductoraDAO.insert(productora1);
        return productora1;
    }

    private ArrayList<Plataforma> mapToPlataforma(String plataforma){
        SQLPlataformaDAO sqlPlataformaDAO = new SQLPlataformaDAO();
        String[] listaPlataformas = plataforma.split(",");
        ArrayList<Plataforma> plataformas = new ArrayList<>();
        for (int i = 0; i < listaPlataformas.length; i++) {
            Plataforma plataforma1 = sqlPlataformaDAO.findByName(listaPlataformas[i].trim());
            if (plataforma1 == null) {
                plataforma1 = new Plataforma(-1,listaPlataformas[i].trim());
                sqlPlataformaDAO.insert(plataforma1);
            }
            plataformas.add(plataforma1);
        }
        return plataformas;
    }

}
