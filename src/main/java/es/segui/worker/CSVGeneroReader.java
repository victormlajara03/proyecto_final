package es.segui.worker;

import es.segui.modelo.entidades.Genero;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CSVGeneroReader {

    private final String DATABASE_FILE = "generos.csv";

    private File fileDatabase;

    public CSVGeneroReader() {
        ClassLoader classLoader = getClass().getClassLoader();
        fileDatabase = new File(classLoader.getResource(DATABASE_FILE).getFile());
    }

    public ArrayList<Genero> findAll() {
        ArrayList<Genero> generos = new ArrayList<>();
        boolean firstLine = true;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileDatabase))){
            do {
                String generosRegister = bufferedReader.readLine();
                if (generosRegister == null) {
                    return generos;
                }
                if (firstLine) {
                    firstLine = false;
                } else {
                    Genero genero = mapToGenero(generosRegister);
                    generos.add(genero);
                }
            } while (true);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return generos;
    }

    private Genero mapToGenero(String line) {
        String[] datosGenero = line.split(",");
        int id = Integer.parseInt(datosGenero[0].trim());
        String cod = datosGenero[1].trim();
        String descripcion = datosGenero[2].trim();
        return new Genero(id,cod,descripcion);
    }
}
