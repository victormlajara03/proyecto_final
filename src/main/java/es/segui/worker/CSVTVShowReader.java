package es.segui.worker;

import es.segui.modelo.dao.SQLProduccionDAO;
import es.segui.modelo.entidades.produccion.Produccion;
import es.segui.modelo.entidades.produccion.Serie;
import es.segui.modelo.entidades.Temporada;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.Year;
import java.util.ArrayList;

public class CSVTVShowReader {

    private final String DATABASE_FILE = "temporadas.csv";

    private File fileDatabase;

    public CSVTVShowReader() {
        ClassLoader classLoader = getClass().getClassLoader();
        fileDatabase = new File(classLoader.getResource(DATABASE_FILE).getFile());
    }

    public ArrayList<Temporada> findAll() {
        ArrayList<Temporada> temporadas = new ArrayList<>();
        boolean firstLine = true;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileDatabase))){
            do {
                String temporadasRegister = bufferedReader.readLine();
                if (temporadasRegister == null) {
                    return temporadas;
                }
                if (firstLine) {
                    firstLine = false;
                } else {
                    Temporada temporada = mapToTemporada(temporadasRegister);
                    temporadas.add(temporada);
                }
            } while (true);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return temporadas;
    }

    private Temporada mapToTemporada(String line) {
        String[] datosTemporada = line.split(";");
        Produccion produccion = mapToSerie(datosTemporada[0].trim());
        int idTemporada = Integer.parseInt(datosTemporada[1].trim());
        int anyoLanzamiento = Integer.parseInt(datosTemporada[2].trim());
        String guion = datosTemporada[3].trim();
        int numCapitulos = Integer.parseInt(datosTemporada[4].trim());
        return new Temporada(produccion,idTemporada,anyoLanzamiento,guion,numCapitulos);

    }

    private Produccion mapToSerie(String line) {
        SQLProduccionDAO sqlProduccionDAO = new SQLProduccionDAO();
        return sqlProduccionDAO.findById(Integer.parseInt(line));
    }
}
