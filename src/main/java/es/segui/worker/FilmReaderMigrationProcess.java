package es.segui.worker;

import es.segui.modelo.dao.*;
import es.segui.modelo.entidades.Genero;
import es.segui.modelo.entidades.Idioma;
import es.segui.modelo.entidades.Temporada;
import es.segui.modelo.entidades.produccion.Produccion;

import java.util.ArrayList;

public class FilmReaderMigrationProcess {

    public static void main(String[] args) {
        CSVGeneroReader csvGeneroReader = new CSVGeneroReader();
        ArrayList<Genero> generos = csvGeneroReader.findAll();
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        for (int i = 0; i < generos.size(); i++) {
            sqlGeneroDAO.insert(generos.get(i));
        }

        SQLIdiomaDAO sqlIdiomaDAO = new SQLIdiomaDAO();
        CSVLocaleReader csvLocaleReader = new CSVLocaleReader();
        ArrayList<Idioma> idiomas = csvLocaleReader.findAll();
        for (int i = 0; i < idiomas.size(); i++) {
            sqlIdiomaDAO.insert(idiomas.get(i));
        }

        CSVFilmReader csvFilmReader = new CSVFilmReader();
        ArrayList<Produccion> producciones = csvFilmReader.findAll();
        SQLProduccionDAO sqlProduccionDAO = new SQLProduccionDAO();
        SQLActorDAO sqlActorDAO = new SQLActorDAO();
        SQLDirectorDAO sqlDirectorDAO = new SQLDirectorDAO();
        SQLPlataformaDAO sqlPlataformaDAO = new SQLPlataformaDAO();
        for (int i = 0; i < producciones.size(); i++) {
            sqlProduccionDAO.insertProduccion(producciones.get(i));
            for (int j = 0; j < producciones.get(i).getDirector().size(); j++) {
                sqlDirectorDAO.insertRelation(producciones.get(i).getId(),producciones.get(i).getDirector().get(j).getCod());
            }

            for (int k = 0; k < producciones.get(i).getActor().size(); k++) {
                sqlActorDAO.insertRelation(producciones.get(i).getId(),producciones.get(i).getActor().get(k).getCod());
            }
            for (int l = 0; l < producciones.get(i).getPlataforma().size(); l++) {
                sqlPlataformaDAO.insertRelation(producciones.get(i).getId(),producciones.get(i).getPlataforma().get(l).getCod());
            }

            for (int m = 0; m < producciones.get(i).getGenero().size(); m++) {
                sqlGeneroDAO.insertRelation(producciones.get(i).getId(),producciones.get(i).getGenero().get(m).getId());
            }

            for (int n = 0; n < producciones.get(i).getIdioma().size(); n++) {
                sqlIdiomaDAO.insertRelation(producciones.get(i).getId(),producciones.get(i).getIdioma().get(n).getId());
            }
        }

        CSVTVShowReader csvtvShowReader = new CSVTVShowReader();
        ArrayList<Temporada> temporadas = csvtvShowReader.findAll();
        SQLTemporadaDAO sqlTemporadaDAO = new SQLTemporadaDAO();
        for (int i = 0; i < temporadas.size(); i++) {
            sqlTemporadaDAO.insert(temporadas.get(i));
        }
    }
}
