package es.segui.controlador;
import es.segui.modelo.dao.SQLProduccionDAO;
import es.segui.modelo.dao.SQLValorarDAO;
import es.segui.modelo.entidades.produccion.Produccion;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ValorarServlet", value = "/valorar")
public class ValorarServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SQLProduccionDAO sqlProduccionDAO = new SQLProduccionDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        Produccion produccion= sqlProduccionDAO.findBy("id",id);
        if (produccion != null){
            sqlProduccionDAO.completeProduction(produccion);
        }
        request.setAttribute("produccion",produccion);
        request.getRequestDispatcher("/detail.jsp").include(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String correo = request.getParameter("correo");
        int id = Integer.parseInt(request.getParameter("id"));
        int valoracion = Integer.parseInt(request.getParameter("estrellas"));
        SQLValorarDAO sqlValorarDAO = new SQLValorarDAO();
        sqlValorarDAO.save(correo,id,valoracion);
        response.sendRedirect("show-details");
    }
}
