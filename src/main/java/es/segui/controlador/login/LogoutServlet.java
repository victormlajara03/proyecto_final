package es.segui.controlador.login;

import com.mysql.cj.Session;
import es.segui.controlador.PasswordHelper;
import es.segui.modelo.dao.SQLGeneroDAO;
import es.segui.modelo.dao.SQLUserDAO;
import es.segui.modelo.entidades.Genero;
import es.segui.modelo.entidades.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "LogoutServlet", value = "/logout")
public class LogoutServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getSession().invalidate();
        response.sendRedirect("lista-producciones");
    }

}
