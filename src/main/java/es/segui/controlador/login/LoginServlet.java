package es.segui.controlador.login;

import es.segui.controlador.PasswordHelper;
import es.segui.modelo.dao.SQLGeneroDAO;
import es.segui.modelo.dao.SQLUserDAO;
import es.segui.modelo.entidades.Genero;
import es.segui.modelo.entidades.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "LoginServlet", value = "/login")
public class LoginServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        ArrayList<Genero> generos = sqlGeneroDAO.findAll();
        request.setAttribute("generos",generos);
        request.getRequestDispatcher("/login.jsp").include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = (req.getParameter("email") != null) ? req.getParameter("email").trim() : "";
        String password = (req.getParameter("password") != null) ? req.getParameter("password").trim() : "";
        String message;
        Usuario usuario = null;
        if (email.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
            message = "You must include username and password";
            req.getSession().setAttribute("message", message);
            resp.sendRedirect("login");
        } else {
            SQLUserDAO sqlUserDAO = new SQLUserDAO();
            Usuario user = sqlUserDAO.findBy("correo",email);
            if (user == null) {
                message = "The user account does not exists";
                req.getSession().setAttribute("message",message);
                resp.sendRedirect("login");
            } else {
                boolean isCorrect = PasswordHelper.verify(password, user.getPassword());
                if (isCorrect) {
                    message = "User login correcto";
                    usuario = user;
                    req.getSession().setAttribute("usuario",usuario);
                    resp.sendRedirect("lista-producciones");
                } else {
                    message = "Password introducido es incorrecto";
                    req.getSession().setAttribute("message",message);
                    resp.sendRedirect("login");
                }
            }
        }
    }




}
