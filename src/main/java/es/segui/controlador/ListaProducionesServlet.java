package es.segui.controlador;

import es.segui.modelo.dao.SQLFavoritoDAO;
import es.segui.modelo.dao.SQLGeneroDAO;
import es.segui.modelo.dao.SQLProduccionDAO;
import es.segui.modelo.dao.SQLValorarDAO;
import es.segui.modelo.entidades.Genero;
import es.segui.modelo.entidades.Usuario;
import es.segui.modelo.entidades.produccion.Produccion;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "ListaProducionesServlet", value = "/lista-producciones")
public class ListaProducionesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SQLProduccionDAO sqlProduccionDAO = new SQLProduccionDAO();
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        ArrayList<Produccion> produccions = sqlProduccionDAO.findTop("movie");
        ArrayList<Produccion> series = sqlProduccionDAO.findTop("tvshow");
        Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
        ArrayList<Genero> generos = sqlGeneroDAO.findAll();
        if (usuario != null) {
            SQLFavoritoDAO sqlFavoritoDAO = new SQLFavoritoDAO();
            ArrayList<Integer> listadoFavoritos = sqlFavoritoDAO.findIdProdFav(usuario.getEmail());
            request.setAttribute("listadoFavoritos",listadoFavoritos);
        }
        SQLValorarDAO sqlValorarDAO = new SQLValorarDAO();
        HashMap<Integer,Float> scores  = sqlValorarDAO.obtenerPuntuacionMedia();
        request.setAttribute("scores ",scores );
        request.setAttribute("peliculas",produccions);
        request.setAttribute("series",series);
        request.setAttribute("generos",generos);
        request.getRequestDispatcher("/suggested.jsp").include(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
