package es.segui.controlador;

import es.segui.modelo.dao.SQLGeneroDAO;
import es.segui.modelo.dao.SQLProduccionDAO;
import es.segui.modelo.dao.SQLTemporadaDAO;
import es.segui.modelo.dao.SQLValorarDAO;
import es.segui.modelo.entidades.Genero;
import es.segui.modelo.entidades.Temporada;
import es.segui.modelo.entidades.Tipo;
import es.segui.modelo.entidades.produccion.Produccion;
import es.segui.modelo.entidades.produccion.Serie;
import es.segui.modelo.repositorio.ProduccionRepositorio;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet(name = "ShowDetailsServlet", value = "/show-details")
public class ShowDetailsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        SQLProduccionDAO sqlProduccionDAO = new SQLProduccionDAO();
        ProduccionRepositorio repositorio = new ProduccionRepositorio();
        Produccion produccion = repositorio.getByIdPopulateAll(id);
        produccion.setNumVisitas();
        sqlProduccionDAO.updateVisitas(produccion);
        ArrayList<Genero> generos = sqlGeneroDAO.findAll();
        request.setAttribute("generos",generos);
        if (produccion instanceof Serie){
            int pagina = Integer.parseInt(request.getParameter("page"));
            SQLTemporadaDAO sqlTemporadaDAO = new SQLTemporadaDAO();
            ArrayList<Temporada> temporadas = sqlTemporadaDAO.findAllBy("id",String.valueOf(produccion.getId()));
            request.setAttribute("pagina",pagina);
            request.setAttribute("temporadas",temporadas);
        }
        SQLValorarDAO sqlValorarDAO = new SQLValorarDAO();
        HashMap<Integer,Float> scores  = sqlValorarDAO.obtenerPuntuacionMedia();
        request.setAttribute("scores ",scores );
        request.setAttribute("pageTemporada",0);
        request.setAttribute("produccion",produccion);
        request.getRequestDispatcher("/detail.jsp").include(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String correo = request.getParameter("correo");
        int id = Integer.parseInt(request.getParameter("id"));

        Integer valoracion = 0;
        if (request.getParameter("estrellas") != null){
            valoracion =  Integer.parseInt(request.getParameter("estrellas"));
        }
        SQLValorarDAO sqlValorarDAO = new SQLValorarDAO();
        sqlValorarDAO.save(correo,id,valoracion);
        String enlace = request.getParameter("enlace");
        response.sendRedirect("show-details?" + enlace);
    }
}
