package es.segui.controlador;

import es.segui.modelo.dao.SQLFavoritoDAO;
import es.segui.modelo.dao.SQLGeneroDAO;
import es.segui.modelo.dao.SQLProduccionDAO;
import es.segui.modelo.dao.SQLValorarDAO;
import es.segui.modelo.entidades.Genero;
import es.segui.modelo.entidades.Usuario;
import es.segui.modelo.entidades.produccion.Produccion;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet(name = "BusquedaServlet", value = "/busqueda-filtrada")
public class BusquedaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SQLProduccionDAO sqlProduccionDAO = new SQLProduccionDAO();
        String tipo = request.getParameter("tipo");
        String orden = request.getParameter("orden");
        ArrayList<Produccion> list;
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        ArrayList<Genero> generos = sqlGeneroDAO.findAll();
        SQLValorarDAO sqlValorarDAO = new SQLValorarDAO();
        int numberProductions ;
        int page = Integer.parseInt(request.getParameter("page"));
        int offset = page * 8;
        if (tipo != null){
            HashMap<String,String> params = new HashMap<>();
            params.put("tipo",tipo);
            list= sqlProduccionDAO.findAllWithParams(params,8,offset,orden);
            numberProductions =  sqlProduccionDAO.cantidadBuscada(params);
        } else {
            String search = request.getParameter("search");
            String genero = request.getParameter("tematica");
            if (!genero.equals("")){
                list = sqlProduccionDAO.findByTituloAndGener(search,genero,8,offset,orden);
            } else {
                list = sqlProduccionDAO.findByTituloAndGener(search,8,offset,orden);
            }
            tipo = "Resultados";
            numberProductions =  sqlProduccionDAO.countFindByTituloAndGener(search,genero);
        }
        int paginas = numberProductions/8;
        if (numberProductions%8 != 0){
            paginas++;
        }
        Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
        if (usuario != null) {
            SQLFavoritoDAO sqlFavoritoDAO = new SQLFavoritoDAO();
            ArrayList<Integer> listadoFavoritos = sqlFavoritoDAO.findIdProdFav(usuario.getEmail());
            request.setAttribute("listadoFavoritos",listadoFavoritos);
        }
        HashMap<Integer,Float> scores  = sqlValorarDAO.obtenerPuntuacionMedia();
        request.setAttribute("scores",scores );
        request.setAttribute("lista",list);
        request.setAttribute("tipo",tipo);
        request.setAttribute("generos",generos);
        request.setAttribute("paginas",paginas);
        request.setAttribute("enlace","busqueda-filtrada?");
        request.getRequestDispatcher("/search.jsp").include(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
