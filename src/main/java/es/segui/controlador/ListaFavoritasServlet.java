package es.segui.controlador;

import es.segui.modelo.dao.SQLFavoritoDAO;
import es.segui.modelo.dao.SQLGeneroDAO;
import es.segui.modelo.dao.SQLProduccionDAO;
import es.segui.modelo.dao.SQLValorarDAO;
import es.segui.modelo.entidades.Genero;
import es.segui.modelo.entidades.Usuario;
import es.segui.modelo.entidades.produccion.Produccion;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet(name = "ListaFavoritasServlet", value = "/lista-favoritas")
public class ListaFavoritasServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SQLProduccionDAO sqlProduccionDAO = new SQLProduccionDAO();
        String orden = request.getParameter("orden");
        Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
        int page = Integer.parseInt(request.getParameter("page"));
        int offset = page * 8;
        ArrayList<Produccion> list = sqlProduccionDAO.findByFavoritos(8,offset,orden, usuario.getEmail());
        SQLGeneroDAO sqlGeneroDAO = new SQLGeneroDAO();
        ArrayList<Genero> generos = sqlGeneroDAO.findAll();
        SQLValorarDAO sqlValorarDAO = new SQLValorarDAO();
        HashMap<Integer,Float> scores  = sqlValorarDAO.obtenerPuntuacionMedia();
        SQLFavoritoDAO sqlFavoritoDAO = new SQLFavoritoDAO();
        ArrayList<Integer> listadoFavoritos = sqlFavoritoDAO.findIdProdFav(usuario.getEmail());
        int numberProductions = sqlFavoritoDAO.countFav(usuario.getEmail());
        int paginas = numberProductions/8;
        if (numberProductions%8 != 0){
            paginas++;
        }
        request.setAttribute("listadoFavoritos",listadoFavoritos);
        request.setAttribute("scores",scores );
        request.setAttribute("lista",list);
        request.setAttribute("tipo","Favoritos");
        request.setAttribute("generos",generos);
        request.setAttribute("paginas",paginas);
        request.setAttribute("enlace","lista-favoritas?");
        request.getRequestDispatcher("/search.jsp").include(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
