package es.segui.controlador;

import es.segui.modelo.dao.SQLFavoritoDAO;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "FavoritosServlet", value = "/favoritos")
public class FavoritosServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/suggested.jsp").include(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String correo = request.getParameter("correo");
        int id = Integer.parseInt(request.getParameter("id-fav"));
        SQLFavoritoDAO sqlFavoritoDAO = new SQLFavoritoDAO();
        sqlFavoritoDAO.save(correo,id);
        response.sendRedirect(request.getHeader("Referer"));
    }
}
