package es.segui.controlador;
import org.apache.commons.codec.digest.DigestUtils;

public class PasswordHelper {

    /**
     * Generate hash code from String
     *
     * @return hash generated
     */
    public static String generateSha1(String password){

        return DigestUtils.sha1Hex(password);

    }

    /**
     * Check password match with hashCode
     *
     * @return boolean
     */
    public static boolean verify(String password, String hashCode){

        return generateSha1(password).equals(hashCode);

    }

}
