
CREATE DATABASE IF NOT EXISTS BATOI_FLIX;
USE BATOI_FLIX;

CREATE TABLE DIRECTOR(

	id INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(50) NOT NULL
);

CREATE TABLE ACTOR(

	id INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(50) NOT NULL
);

CREATE TABLE IDIOMA(

	id INT PRIMARY KEY,
	cod VARCHAR(20) NOT NULL,
	nombre VARCHAR(100)
);

CREATE TABLE ESCRITOR(

	id INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(50) NOT NULL
);

CREATE TABLE PRODUCTORA(

	id INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(50) NOT NULL
	
);

CREATE TABLE PLATAFORMA(

	id INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(50)
);

CREATE TABLE GENERO(
	
	id INT PRIMARY KEY,
	cod VARCHAR(10) NOT NULL,
	descripcion VARCHAR(20) NOT NULL
	
);

CREATE TABLE CALIFICACION(

	id INT AUTO_INCREMENT PRIMARY KEY,
	calificacion ENUM("G", "PG","PG-13","R", "X", "UNRATED","APPROVED") NOT NULL
);

CREATE TABLE PRODUCCION(

	id INT PRIMARY KEY,
	titulo VARCHAR(50) NOT NULL,
	calificacion INT NOT NULL,
	flanzamiento DATE NOT NULL,
	duracion INT ,
	guion VARCHAR(800) NOT NULL,
	portada VARCHAR(200) NOT NULL,
	tipo ENUM("tvshow", "movie")NOT NULL,
	productora INT NOT NULL,
	url_trailer VARCHAR(220),
	
	FOREIGN KEY (calificacion) REFERENCES CALIFICACION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY (productora) REFERENCES PRODUCTORA(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT

);

CREATE TABLE TEMPORADA(

	id INT,
	id_temporada INT,
	fecha_lanzamiento DATE NOT NULL,
	plot VARCHAR(600) NOT NULL,
	capitulos INT NOT NULL,
	
	PRIMARY KEY(id, id_temporada),
	FOREIGN KEY(id) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE USUARIO(

	correo VARCHAR(50) PRIMARY KEY,
	nombre VARCHAR(20) NOT NULL,
	password VARCHAR(64) NOT NULL,
	tipo ENUM("invitado", "logueado") NOT NULL
);

CREATE TABLE FAVORITO(

	correo VARCHAR(50),
	id_produccion INT,
	
	PRIMARY KEY(correo, id_produccion),
	FOREIGN KEY(correo) REFERENCES USUARIO(correo)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE PENDIENTE(

	correo VARCHAR(50),
	id_produccion INT,
	
	PRIMARY KEY(correo, id_produccion),
	FOREIGN KEY(correo) REFERENCES USUARIO(correo)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE VISTO(

	correo VARCHAR(50),
	id_produccion INT,
	
	PRIMARY KEY(correo, id_produccion),
	FOREIGN KEY(correo) REFERENCES USUARIO(correo)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE VALORAR(

	correo VARCHAR(50),
	id_produccion INT,
	puntuacion INT NOT NULL,
	
	PRIMARY KEY(correo, id_produccion),
	FOREIGN KEY(correo) REFERENCES USUARIO(correo)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
); 

CREATE TABLE PRODUCCION_GENERO(
	
	id_genero INT,
	id_produccion INT,
	
	PRIMARY KEY(id_genero, id_produccion),
	FOREIGN KEY(id_genero) REFERENCES GENERO(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT

);

CREATE TABLE PRODUCCION_PLATAFORMA(

	id_plataforma INT,
	id_produccion INT,
	
	PRIMARY KEY(id_plataforma, id_produccion),
	
	FOREIGN KEY(id_plataforma) REFERENCES PLATAFORMA(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE PRODUCCION_IDIOMA(

	id_idioma INT,
	id_produccion INT,
	
	PRIMARY KEY(id_idioma, id_produccion),
	FOREIGN KEY(id_idioma) REFERENCES IDIOMA(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE PRODUCCION_ESCRITOR(

	id_escritor INT,
	id_produccion INT,
	
	PRIMARY KEY(id_escritor, id_produccion),
	
	FOREIGN KEY(id_escritor) REFERENCES ESCRITOR(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE PRODUCCION_ACTOR(

	id_actor INT,
	id_produccion INT,
	
	PRIMARY KEY(id_actor, id_produccion),
	
	FOREIGN KEY(id_actor) REFERENCES ACTOR(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE PRODUCCION_DIRECTOR(

	id_director INT,
	id_produccion INT,
	
	PRIMARY KEY(id_director, id_produccion),
	FOREIGN KEY(id_director) REFERENCES DIRECTOR(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(id_produccion) REFERENCES PRODUCCION(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);